# ciceksepeti-hackathon-step-one
Hello dear reviewer :) I want to say something for you..
I wish I had enough poison for the whole pack of code smells :)
I think you know that, if you stressed by deadline, project goes bad.
For this case, even Master Yoda would say that:

> Your project in chaos, will be..

In addition: I wanted to add specific documentation about code blocks and methods, also wanted to write tests but I didn't manage my time to do all this.
#Installation
A step by step intallation guide

Clone the project

```
git clone https://btuerker@bitbucket.org/btuerker/ciceksepeti-hackathon-step-one.git
```

Move into project's folder

```
cd ciceksepet-hackthon-step-one
```

Build project via Maven as fat jar and launch

```
mvn package && java -jar target/ciceksepeti-hackathon-step-one-0.0.1-SNAPSHOT.jar 
```

#Live Preview
[Heroku Deployment](https://pure-tor-11203.herokuapp.com/) 