package com.btuerker;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import com.btuerker.payload.Dealer;
import com.btuerker.payload.Location;
import com.btuerker.payload.Order;
import com.btuerker.service.DistributionService;
import com.btuerker.util.OrderBSTContainer;
import com.google.gson.Gson;

@Controller
@SpringBootApplication
public class WebApplicationLauncher {
	@Autowired
	private ResourceLoader resourceLoader;
	private final static String ORDERS_AND_DEALERS_FILE_NAME = "siparis-bayi-koordinatlari.xlsx";
	private static List<Order> orders = new ArrayList<>();
	private static List<Dealer> dealers = new ArrayList<>();

	@GetMapping("/")
	public String getData(Model model) {
		Gson gson = new Gson();
		model.addAttribute("dealers", gson.toJson(this.dealers));
		model.addAttribute("orders", gson.toJson(this.orders));
		model.addAttribute("dealersList", dealers);
		return "index";
	}

	public static void main(String[] args) {
		SpringApplication.run(WebApplicationLauncher.class, args);
		DistributionService distributionService = new DistributionService(orders);
		for (Dealer dealer : dealers) {
			System.out.println(dealer.toString());
			distributionService.addDealer(dealer);
		}
		for (OrderBSTContainer container : distributionService.getContainers()) {
			for (Order o : orders) {
				container.insert(o);
			}
		}
		distributionService.distribute();
	}

	@Bean
	public CommandLineRunner readOrdersFromExcel() {

		return args -> {
			Workbook workbook = null;
			try {
				Resource resource = resourceLoader.getResource("classpath:" + ORDERS_AND_DEALERS_FILE_NAME);
				InputStream fileStream = resource.getInputStream();  
				workbook = new XSSFWorkbook(fileStream);
				Sheet datatypeSheet = workbook.getSheetAt(0);
				Iterator<Row> iterator = datatypeSheet.iterator();
				while (iterator.hasNext()) {
					Row currentRow = iterator.next();
					// skip header row
					if (currentRow.getRowNum() == 0)
						continue;
					Long id = Long.parseLong(currentRow.getCell(0).getStringCellValue());
					Double latitude = Double.parseDouble(currentRow.getCell(1).getStringCellValue());
					Double longtitude = Double.parseDouble(currentRow.getCell(2).getStringCellValue());
					Location location = new Location(latitude, longtitude);
					Order order = new Order(id, location);
					orders.add(order);
				}
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				workbook.close();
			}
		};
	}

	@Bean
	public CommandLineRunner readDealersFromExcel() {
		return args -> {
			Workbook workbook = null;
			try {
				Resource resource = resourceLoader.getResource("classpath:" + ORDERS_AND_DEALERS_FILE_NAME);
				InputStream fileStream = resource.getInputStream();  
				workbook = new XSSFWorkbook(fileStream);
				Sheet datatypeSheet = workbook.getSheetAt(1);
				Iterator<Row> iterator = datatypeSheet.iterator();
				while (iterator.hasNext()) {
					Row currentRow = iterator.next();
					// skip header row
					if (currentRow.getRowNum() == 0)
						continue;
					String name = currentRow.getCell(0).getStringCellValue();
					Double latitude = Double.parseDouble(currentRow.getCell(1).getStringCellValue());
					Double longtitude = Double.parseDouble(currentRow.getCell(2).getStringCellValue());
					Location location = new Location(latitude, longtitude);
					Dealer dealer = new Dealer(name, location);
					dealers.add(dealer);
				}
				dealers.get(0).setMin(20);
				dealers.get(0).setMax(30);
				dealers.get(1).setMin(35);
				dealers.get(1).setMax(50);
				dealers.get(2).setMin(20);
				dealers.get(2).setMax(80);
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} finally {
				workbook.close();
			}
		};
	}
}