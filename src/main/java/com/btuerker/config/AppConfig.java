package com.btuerker.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Scope;

import com.btuerker.payload.Location;
import com.btuerker.payload.Order;

@Configuration
@ComponentScan("com.btuerker")
public class AppConfig {
	
	@Bean
	public Order getOrder() {
		return new Order();
	}
	
	@Bean
	@Scope()
	public Location getLocation() {
		return new Location();
	}
}
