package com.btuerker.payload;

import java.util.ArrayList;
import java.util.List;

public class Dealer {
	public String name;
	private Location location;
	private int min;
	private int max;
	private List<Order> orders = new ArrayList<>();

	public Dealer(String name, Location location) {
		this.name = name;
		this.location = location;
	}

	public Double getDistanceFrom(Location location) {
		return this.location.getDistanceFrom(location);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	public int getMin() {
		return min;
	}

	public void setMin(int min) {
		this.min = min;
	}

	public int getMax() {
		return max;
	}

	public void setMax(int max) {
		this.max = max;
	}

	public List<Order> getOrders() {
		return orders;
	}

	public void setOrders(List<Order> orders) {
		this.orders = orders;
	}

	public String toString() {
		return "{name:" + this.name + ",location:" + this.location.toString() + 
				",min:" + this.min + ",max:" + this.max + 
				",orders" + this.orders.toString() + "}";
	}

	public boolean addOrder(Order o) {
		return this.orders.add(o);
	}
	
	public boolean removeOrder(Order o) {
		return this.orders.remove(o);
	}
}
