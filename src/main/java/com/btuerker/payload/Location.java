package com.btuerker.payload;

public class Location {
	private double latitude;
	private double longtitude;

	public Location() {
	}

	public Location(double latitude, double longtitude) {
		super();
		this.latitude = latitude;
		this.longtitude = longtitude;
	}

	public double getLatitude() {
		return latitude;
	}

	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}

	public double getLongtitude() {
		return longtitude;
	}

	public void setLongtitude(double longtitude) {
		this.longtitude = longtitude;
	}

	public double getDistanceFrom(Location other) {
		return Math.sqrt(Math.pow(this.latitude - other.latitude, 2) + Math.pow(this.longtitude - other.longtitude, 2));
	}

	@Override
	public String toString() {
		return "{latitude:" + this.latitude + "," + "longtitude:" + this.longtitude + "}";
	}

	@Override
	public boolean equals(Object other) {
		// TODO Auto-generated method stub
		if (other instanceof Location) {
			Location location = (Location) other;
			return (this.getLatitude() == location.getLatitude() && this.getLongtitude() == location.getLongtitude());
		}
		return false;
	}
}
