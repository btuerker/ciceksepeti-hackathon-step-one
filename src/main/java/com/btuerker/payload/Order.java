package com.btuerker.payload;

public class Order {
	private Long id;
	private Location location;

	public Order() {

	}

	public Order(Long id, Location location) {
		this.id = id;
		this.location = location;
	}

	public Double getDistanceFrom(Location location) {
		return this.location.getDistanceFrom(location);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Location getLocation() {
		return location;
	}

	public void setLocation(Location location) {
		this.location = location;
	}

	@Override
	public String toString() {
		return "{id:" + this.id + ",location:" + this.location.toString() + "}";
	}
}