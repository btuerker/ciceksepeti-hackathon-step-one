package com.btuerker.payload;

public enum OrderStatus {
	ACCEPTED, REJECTED, IN_PROGRESS 
}