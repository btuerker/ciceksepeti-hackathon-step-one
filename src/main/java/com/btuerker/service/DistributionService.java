package com.btuerker.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.btuerker.payload.Dealer;
import com.btuerker.payload.Location;
import com.btuerker.payload.Order;
import com.btuerker.util.OrderBSTContainer;

public class DistributionService {
	private List<OrderBSTContainer> containers = new ArrayList<>();
	private List<Order> orders = new ArrayList<>();

	public DistributionService(List<Order> orders) {
		this.orders = orders;
	}

	public void pushOrder(Order order) {
		for (OrderBSTContainer c : containers) {
			c.insert(order);
		}
	}

	public void distribute() {
		distributeInMinimumOrder();
		distributeLeftBehind();
	}

	private void distributeInMinimumOrder() {
		for (OrderBSTContainer container : containers) {
			Dealer currentDealer = container.getDealer();
			while (currentDealer.getOrders().size() < currentDealer.getMin()) {
				Order nearestOrder = container.getNearest();
				while (!orders.contains(nearestOrder)) {
					container.deleteMin();
					nearestOrder = container.getNearest();
				}
//				compare with distance!!! not with name cause there would be repeated values
				Dealer nearestDealer = getNearestDealerFrom(nearestOrder);
				if (currentDealer == nearestDealer) {
					currentDealer.addOrder(nearestOrder);
					container.deleteMin();
					orders.remove(nearestOrder);
				} else {
					OrderBSTContainer c = new OrderBSTContainer(currentDealer);
					for (Order o : nearestDealer.getOrders()) {
						c.insert(o);
					}
					if (currentDealer.getDistanceFrom(c.getNearest().getLocation()) 
							< currentDealer.getDistanceFrom(nearestOrder.getLocation())) {
						currentDealer.addOrder(c.getNearest());
						nearestDealer.removeOrder(c.getNearest());
						nearestDealer.addOrder(nearestOrder);
						orders.remove(nearestOrder);
					}
				}
			}
		}
	}

	private void distributeLeftBehind() {
		for (OrderBSTContainer container : containers) {
			Dealer currentDealer = container.getDealer();
			while (currentDealer.getOrders().size() < currentDealer.getMax()) {
				Order nearestOrder = container.getNearest();
				while (!orders.contains(nearestOrder)) {
					container.deleteMin();
					nearestOrder = container.getNearest();
				}
//				compare with distance!!! not with name cause there would be repeated values
				Dealer nearestDealer = getNearestDealerFrom(nearestOrder);
				if (currentDealer == nearestDealer) {
					currentDealer.addOrder(nearestOrder);
					container.deleteMin();
					orders.remove(nearestOrder);
				} else {
					OrderBSTContainer c = new OrderBSTContainer(currentDealer);
					for (Order o : nearestDealer.getOrders()) {
						c.insert(o);
					}
					if (currentDealer.getDistanceFrom(c.getNearest().getLocation()) 
							< currentDealer.getDistanceFrom(nearestOrder.getLocation())) {
						currentDealer.addOrder(c.getNearest());
						nearestDealer.removeOrder(c.getNearest());
						nearestDealer.addOrder(nearestOrder);
						orders.remove(nearestOrder);
					}
				}
			}
		}
	}

	private Dealer getNearestDealerFrom(Order order) {
		Dealer nearestDealer = null;
		Double minDistance = null;
		for (OrderBSTContainer container : containers) {
			Dealer currentDealer = container.getDealer();
			Double currentDistance = order.getDistanceFrom(currentDealer.getLocation());
			if (minDistance == null) {
				nearestDealer = currentDealer;
				minDistance = currentDistance;
				continue;
			}
			if (minDistance > currentDistance) {
				nearestDealer = currentDealer;
				minDistance = currentDistance;
			}
		}
		return nearestDealer;
	}

	public void addDealer(Dealer dealer) {
		containers.add(new OrderBSTContainer(dealer));
	}

	public void removeDealer(Dealer dealer) {
		containers.remove(dealer);
	}

	public List<OrderBSTContainer> getContainers() {
		return containers;
	}

	public void setContainers(List<OrderBSTContainer> containers) {
		this.containers = containers;
	}

}
