package com.btuerker.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;

public class CSVConvertor {
	public static void echoAsCSV(Sheet sheet) throws IOException {
		Row row = null;
		try (PrintWriter writer = new PrintWriter(new File("test.csv"))) {
			StringBuilder sb = new StringBuilder();
			for (int i = 1; i <= sheet.getLastRowNum(); i++) {
				row = sheet.getRow(i);
				for (int j = 1; j < row.getLastCellNum(); j++) {
					sb.append(row.getCell(j));
					if (j != row.getLastCellNum() - 1) sb.append(",");
				}
				sb.append("\n");
			}
			writer.write(sb.toString());
		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}
}
