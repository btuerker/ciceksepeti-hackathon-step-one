package com.btuerker.util;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.btuerker.payload.Dealer;
import com.btuerker.payload.Order;

public class OrderBSTContainer {
	private Dealer dealer;
	private OrderBSTNode<Order> root;
	private Map<Double, Integer> repeatedValues = new HashMap<>();
	public Map<Double, List<OrderBSTNode<Order>>> values = new HashMap<>();

	public OrderBSTContainer(Dealer distributor) {
		this.dealer = distributor;
	}

	public void insert(Order order) {
		OrderBSTNode<Order> newNode = new OrderBSTNode<Order>(order);
		double distanceFromOrder = dealer.getLocation().getDistanceFrom(order.getLocation());
		if (root == null) {
			root = newNode;
			return;
		}
		OrderBSTNode<Order> current = root;
		OrderBSTNode<Order> parent = null;
		while (true) {
			parent = current;
			double distanceFromCurrent = dealer.getDistanceFrom(current.getElement().getLocation());
			if (distanceFromOrder < distanceFromCurrent) {
				current = current.getLeft();
				if (current == null) {
					parent.setLeft(newNode);
					return;
				}
			}
			if (distanceFromCurrent == distanceFromOrder) {
				int count = repeatedValues.containsKey(distanceFromCurrent) ? repeatedValues.get(distanceFromCurrent)
						: 0;
				repeatedValues.put(distanceFromCurrent, count + 1);
//				REFACTORING
//				Extract this block
				List<OrderBSTNode<Order>> l = values.containsKey(distanceFromCurrent) ? values.get(distanceFromCurrent)
						: new ArrayList<>();
				l.add(newNode);
				values.put(distanceFromCurrent, l);
				return;
			}
//			Bigger than current's element
			if (distanceFromOrder > distanceFromCurrent) {
				current = current.getRight();
				if (current == null) {
					parent.setRight(newNode);
					return;
				}
			}
		}
	}

	public void deleteMin() {
		if (this.root == null)
			return;
		OrderBSTNode<Order> current = this.root;
		OrderBSTNode<Order> parent = this.root;
		// if root is only element
		if (this.root.getLeft() == null && this.root.getRight() == null) {
			double distanceFromRoot = this.root.getElement().getDistanceFrom(this.dealer.getLocation());
			List<OrderBSTNode<Order>> l = this.values.get(distanceFromRoot);
			if (l != null) {
				if (l.size() > 0) {
					root.setElement(l.get(0).getElement());
					l.remove(0);
					if (l.size() > 0) {
						this.values.put(distanceFromRoot, l);
					} else {
						this.values.remove(distanceFromRoot);
					}
				}
			} else {
				this.root = null;
			}
			return;
		}
		if (this.root.getLeft() == null) {
			double distanceFromRoot = this.root.getElement().getDistanceFrom(this.dealer.getLocation());
			List<OrderBSTNode<Order>> l = this.values.get(distanceFromRoot);
			if (l != null) {
				if (l.size() > 0) {
					root.setElement(l.get(0).getElement());
					l.remove(0);
					if (l.size() > 0) {
						this.values.put(distanceFromRoot, l);
					} else {
						this.values.remove(distanceFromRoot);
					}
				}
			} else {
				root = root.getRight();
			}
			return;
		} else {
			current = current.getLeft();
		}
		while (true) {
			double distance = current.getElement().getDistanceFrom(this.dealer.getLocation());
			if (current.getLeft() == null) {
				if (current.getRight() == null) {
					if (parent.getLeft() == current) {
						List<OrderBSTNode<Order>> l = this.values.get(distance);
						if (l != null) {
							if (l.size() > 0) {
								parent.getLeft().setElement(l.get(0).getElement());
								l.remove(0);
								if (l.size() > 0) {
									this.values.put(distance, l);
								} else {
									this.values.remove(distance);
								}
							}
						} else {
							parent.setLeft(null);
						}
					}
					return;
				} else {
					if (parent.getLeft() == current) {
						List<OrderBSTNode<Order>> l = this.values.get(distance);
						if (l != null) {
							if (l.size() > 0) {
								parent.getLeft().setElement(l.get(0).getElement());
								l.remove(0);
								if (l.size() > 0) {
									this.values.put(distance, l);
								} else {
									this.values.remove(distance);
								}
							}
						} else {
							parent.setLeft(current.getRight());
						}
					}
					return;
				}
			} else {
				parent = current;
				current = current.getLeft();
			}
		}
	}

	private void removeFromRepeatedValues(double distance) {

	}

	public void displayValues() {
		for (Map.Entry<Double, List<OrderBSTNode<Order>>> entry : this.values.entrySet()) {
			System.out.print(entry.getKey() + ":");
			for (OrderBSTNode<Order> order : entry.getValue()) {
				System.out.print(order.getElement().toString());
			}
			System.out.println();
		}
		System.out.println();
	}

	public void traverse(OrderBSTNode<Order> from) {
		from = (from == null) ? root : from;
		if (root == null)
			return;
		System.out.println(from.getElement().toString());
		if (from.getLeft() != null)
			traverse(from.getLeft());
		if (from.getRight() != null)
			traverse(from.getRight());
	}

	public Order getNearest() {
		if (root == null) {
			return null;
		}
		OrderBSTNode<Order> current = root;
		while (current.getLeft() != null) {
			current = current.getLeft();
		}
		return current.getElement();
	}

	public Order getFurtest() {
		if (root == null) {
			return null;
		}
		OrderBSTNode<Order> current = root;
		while (current.getRight() != null) {
			current = current.getRight();
		}
		return current.getElement();
	}

	public Dealer getDealer() {
		return dealer;
	}

	public void setDealer(Dealer dealer) {
		this.dealer = dealer;
	}

	public OrderBSTNode<Order> getRoot() {
		return root;
	}

	public void setRoot(OrderBSTNode<Order> root) {
		this.root = root;
	}

	public Map<Double, Integer> getRepeatedValues() {
		return repeatedValues;
	}

	public void setRepeatedValues(Map<Double, Integer> repeatedValues) {
		this.repeatedValues = repeatedValues;
	}

//	public static void main(String[] args) {
//		Dealer dealer = new Dealer("hello", new Location(1.0, 1.0));
//		OrderBSTContainer container = new OrderBSTContainer(dealer);
//		Order order1 = new Order(1L, new Location(5.0, 5.0));
//		Order order2 = new Order(2L, new Location(7.0, 7.0));
//		Order order3 = new Order(3L, new Location(8.0, 8.0));
//		Order order4 = new Order(4L, new Location(6.0, 6.0));
//		Order order5 = new Order(5L, new Location(5.0, 5.0));
//		Order order6 = new Order(6L, new Location(7.0, 7.0));
//		Order order7 = new Order(7L, new Location(8.0, 8.0));
//		Order order8 = new Order(8L, new Location(6.0, 6.0));
//		Order order9 = new Order(9L, new Location(3.0, 3.0));
//		Order order10 = new Order(10L, new Location(2.0, 2.0));
//		Order order11 = new Order(11L, new Location(4.0, 4.0));
//		Order order12 = new Order(12L, new Location(3.0, 3.0));
//		Order order13 = new Order(13L, new Location(2.0, 2.0));
//		Order order14 = new Order(14L, new Location(4.0, 4.0));
//		container.insert(order1);
//		container.insert(order2);
//		container.insert(order3);
//		container.insert(order4);
//		container.insert(order5);
//		container.insert(order6);
//		container.insert(order7);
//		container.insert(order8);
//		container.insert(order9);
//		container.insert(order10);
//		container.insert(order11);
//		container.insert(order12);
//		container.insert(order13);
//		container.insert(order14);
//
//		container.displayValues();
//		container.deleteMin();
//		container.deleteMin();
//		container.deleteMin();
//		container.deleteMin();
//		container.deleteMin();
//		container.deleteMin();
//		container.deleteMin();
//		container.deleteMin();
//		container.deleteMin();
//		container.deleteMin();
//		container.deleteMin();
//		container.deleteMin();
//		container.deleteMin();
//		container.deleteMin();
//		
//	}
}
