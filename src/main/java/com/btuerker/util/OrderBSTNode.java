package com.btuerker.util;

public class OrderBSTNode<V>{
	private V element;
	private OrderBSTNode<V> left;
	private OrderBSTNode<V> right;
	
	public OrderBSTNode() {
		
	}
	
	public OrderBSTNode(V element) {
		this.element = element;
		this.left = null;
		this.right = null;
	}

	public V getElement() {
		return element;
	}

	public void setElement(V element) {
		this.element = element;
	}

	public OrderBSTNode<V> getLeft() {
		return left;
	}

	public void setLeft(OrderBSTNode<V> left) {
		this.left = left;
	}

	public OrderBSTNode<V> getRight() {
		return right;
	}

	public void setRight(OrderBSTNode<V> right) {
		this.right = right;
	}
	
	@Override
	public String toString() {
		return this.element.toString();
	}
}
