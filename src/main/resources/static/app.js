var datasetOptions = {	
	label : 'label', 
	data:[],
	pointRadius : 20, 
	pointHoverRadius : 40, 
	pointBackgroundColor : '#00ff00'
};

var ctx = document.getElementById("myChart").getContext('2d');
var scatterChart = new Chart(ctx, {
	type : 'scatter',
	data : {
		datasets : [{
			label : 'Kırmızı',
			data : [],
			pointRadius : 15,
			pointHoverRadius:20,
			pointBackgroundColor : '#ff0000'
		},
		{
			label : 'K-Orders',
			data : [],
			pointRadius : 8,
			pointHoverRadius:12,
			pointStyle: 'circle',
			pointBackgroundColor : '#f76767'
		},
		{
			label : 'Mavi',
			data : [],
			pointRadius : 15,
			pointHoverRadius:20,
			pointBackgroundColor : '#0000ff'
		},
		{
			label : 'M-Orders',
			data : [],
			pointRadius : 8,
			pointHoverRadius:12,
			pointStyle: 'triangle',
			pointBackgroundColor : '#468aea'
		},
		{
			label : 'Yeşil',
			data : [],
			pointRadius : 15,
			pointHoverRadius:20,
			pointBackgroundColor : '#00ff00'
		},
		{
			label : 'Y-Orders',
			data : [],
			pointRadius : 8,
			pointHoverRadius:12,
			pointStyle: 'rect',
			pointBackgroundColor : '#8cea79'
		}
		]
	},
	options : {
		scales : {
			xAxes : [ {
				type : 'linear',
				position : 'bottom'
			} ]
		}
	}
});

function pushToChart(label,lat, long) {
	console.log(label);
	dataset = scatterChart.data.datasets.find((dataset) => { 
		var re = new RegExp(`${dataset.label}`);
		return re.test(label);
	});
	if(dataset != null) dataset.data.push({ x: lat, y: long });
}

function getDatasetOptions(labelName, colorHex) {
	var options = datasetOptions;
	options.label = labelName;
	options.pointBackgroundColor = colorHex;
	return options;
}

function getColorFromLabelName(labelName) {
	color = '#ffff00';
	if(/[Kırmızı]/.test(labelName)) color = '#ff0000';
	if(/[Mavi]/.test(labelName)) color = '#0000ff';
	if(/[Yeşil]/.test(labelName)) color =  '#00ff00';
	return color;
}

function updateChart() {
	scatterChart.update();
}

function getCharCodeArray(s) {
	return s.split('').map((c) => c.charCodeAt(0));
}
